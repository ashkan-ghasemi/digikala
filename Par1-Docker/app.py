# Start app.py 
# import Flask Module and  jsonify function from the Flask package.
from flask import Flask, jsonify
import os
app = Flask(__name__)
# Get the worker ID , retrieve the worker ID from an environment variable named WORKER_ID.
# If the environment variable is not set, the default value 'Unknown' is used.
worker_id = os.environ.get('WORKER_ID', 'Unknown')	

@app.route('/worker')
def get_worker_id():
    return jsonify({'worker_id': worker_id}) 	
if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1')
# End  app.py 