# Answer -Part 1.1
* Install Flask if you haven't already installed it
```
$ pip install flask
```
- Set the WORKER_ID environment variable 

```
$ export WORKER_ID=worker1
```

- run application and see output 
```
$ python app.py

$ curl -IL http://localhost:5000/worker
```
Output : 

- Note: You should see a JSON response containing the worker ID, for example:
```
{"worker_id": "worker1"}
```
# Answer -Part 1.2
```
$ docker build -t flask-app .
$ docker run -d -p 5000:5000 flask-app 

$ curl -IL http://localhost:5000/worker
```

- Note: You should see a JSON response containing the worker ID, for example:
```   
 {"worker_id": "worker1"}
```